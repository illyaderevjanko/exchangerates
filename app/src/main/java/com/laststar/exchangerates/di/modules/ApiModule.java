package com.laststar.exchangerates.di.modules;

import com.laststar.exchangerates.api.Api;
import com.laststar.exchangerates.di.scopes.PresenterScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class ApiModule {

    @Provides
    @PresenterScope
    public Api getApi() {
        return Api.InitApi.create();
    }
}
