package com.laststar.exchangerates.di.components;

import com.laststar.exchangerates.di.modules.ContextModule;
import com.laststar.exchangerates.ui.views.MainActivity;
import com.laststar.exchangerates.di.modules.ConverterModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Component(modules = {ContextModule.class, ConverterModule.class})
@Singleton
public interface AppComponent {
    void inject(MainActivity mainActivity);
}
