package com.laststar.exchangerates.di.components;

import com.laststar.exchangerates.di.modules.ApiModule;
import com.laststar.exchangerates.di.scopes.PresenterScope;
import com.laststar.exchangerates.mvp.presenters.MainAPresenter;

import dagger.Component;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Component(modules = {ApiModule.class})
@PresenterScope
public interface PresenterComponent {
    void inject(MainAPresenter mainAPresenter);
}
