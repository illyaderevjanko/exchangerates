package com.laststar.exchangerates.di.modules;

import android.content.Context;

import com.laststar.exchangerates.tools.Converter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Module
public class ConverterModule {

    @Provides
    @Singleton
    public Converter provideConverter(Context context) {
        return new Converter(context);
    }
}
