package com.laststar.exchangerates.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PresenterScope {
}

