package com.laststar.exchangerates.ui.views;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.laststar.exchangerates.App;
import com.laststar.exchangerates.R;
import com.laststar.exchangerates.models.api.Item;
import com.laststar.exchangerates.models.api.Rates;
import com.laststar.exchangerates.mvp.presenters.MainAPresenter;
import com.laststar.exchangerates.mvp.views_state.MainAVS;
import com.laststar.exchangerates.tools.Converter;
import com.laststar.exchangerates.ui.adapters.MainAdapter;
import com.laststar.exchangerates.ui.tools.VerticalSpaceItemDecoration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.RealmList;

public class MainActivity extends MvpAppCompatActivity implements MainAVS {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    AppCompatTextView toolbarTitle;
    @BindView(R.id.list)
    RecyclerView list;
    private MainAdapter mainAdapter;
    @BindView(R.id.txt_current_time)
    AppCompatTextView txtCurrentTime;

    @Inject
    Converter converter;

    @InjectPresenter
    MainAPresenter presenter;

    private Handler timeHandler;
    private Runnable timeRunnable;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        App.getAppComponent().inject(this);

        initToolbar();
        initList();
        startDisplayTime();
        presenter.getRates(converter.getDDMMYY());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        setStartToolbarTitle();
    }

    private void setStartToolbarTitle() {
        toolbarTitle.setText("Загрузка...");
    }

    private void initList() {
        list.setLayoutManager(new LinearLayoutManager(this));
        list.addItemDecoration(new VerticalSpaceItemDecoration(converter.dpToPx(36), converter.dpToPx(36)));
        mainAdapter = new MainAdapter(this, new RealmList<Item>(), converter);
        list.setAdapter(mainAdapter);
    }

    private void startDisplayTime() {
        timeHandler = new Handler(getMainLooper());
        timeRunnable = new Runnable() {
            @Override
            public void run() {
                txtCurrentTime.setText(new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date()));
                timeHandler.postDelayed(this, 1000);
            }
        };
        timeHandler.post(timeRunnable);
    }

    @Override
    public void setResults(Rates results) {
        mainAdapter.setItemList(results.getItems());
        mainAdapter.notifyDataSetChanged();
        toolbarTitle.setText("Курс валют на: " + converter.getRatesDate(results.getDate()));
    }

    @Override
    public void empty() {

    }

    @OnClick(R.id.btn_refresh)
    void refreshRates() {
        toolbarTitle.setText("Загрузка...");
        if(mainAdapter != null) {
            mainAdapter.setItemList(new RealmList<Item>());
            mainAdapter.notifyDataSetChanged();
        }
        presenter.getRates(converter.getDDMMYY());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        if(timeHandler != null) {
            timeHandler.removeCallbacks(timeRunnable);
        }
    }
}
