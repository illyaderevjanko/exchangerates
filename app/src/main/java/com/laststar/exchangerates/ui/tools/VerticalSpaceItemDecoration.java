package com.laststar.exchangerates.ui.tools;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int verticalSpaceHeight;
    private final int verticalSpaceHeightLast;
    private final int verticalSpaceHeightFirst;

    public VerticalSpaceItemDecoration(int verticalSpaceHeight, int verticalSpaceHeightFirst) {
        this.verticalSpaceHeight = verticalSpaceHeight;
        this.verticalSpaceHeightFirst = verticalSpaceHeightFirst;
        this.verticalSpaceHeightLast = 0;
    }

    public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
        this.verticalSpaceHeight = verticalSpaceHeight;
        this.verticalSpaceHeightFirst = 0;
        this.verticalSpaceHeightLast = 0;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getChildAdapterPosition(view) == 0) {
            outRect.top = verticalSpaceHeightFirst;
        }
        if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
            outRect.bottom = verticalSpaceHeight;
        } else {
            outRect.bottom = verticalSpaceHeightLast;
        }
    }
}
