package com.laststar.exchangerates.ui.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.laststar.exchangerates.R;
import com.laststar.exchangerates.models.api.Item;
import com.laststar.exchangerates.tools.Converter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private List<Item> itemList;
    private LayoutInflater layoutInflater;
    private Converter converter;

    public MainAdapter(Context context, List<Item> itemList, Converter converter) {
        this.itemList = itemList;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.converter = converter;
    }

    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainAdapter.ViewHolder(layoutInflater.inflate(R.layout.item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(MainAdapter.ViewHolder holder, int position) {
        Item item = itemList.get(position);
        holder.txtRatesName.setText(item.getName());
        holder.txtBuyDollar.setText(converter.buyText(item.getBuyUSD()));
        holder.txtSellDollar.setText(converter.sellText(item.getSellUSD()));
        holder.txtBuyEuro.setText(converter.buyText(item.getBuyEUR()));
        holder.txtSellEuro.setText(converter.buyText(item.getSellEUR()));
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_rates_name)
        AppCompatTextView txtRatesName;
        @BindView(R.id.txt_buy_dollar)
        AppCompatTextView txtBuyDollar;
        @BindView(R.id.txt_sell_dollar)
        AppCompatTextView txtSellDollar;
        @BindView(R.id.txt_buy_euro)
        AppCompatTextView txtBuyEuro;
        @BindView(R.id.txt_sell_euro)
        AppCompatTextView txtSellEuro;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
