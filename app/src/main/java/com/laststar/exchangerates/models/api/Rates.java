package com.laststar.exchangerates.models.api;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class Rates extends RealmObject {
    private RealmList<Item> items;
    private String date;

    public RealmList<Item> getItems() {
        return items;
    }

    public void setItems(RealmList<Item> items) {
        this.items = items;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
