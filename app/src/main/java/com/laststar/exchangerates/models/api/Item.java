package com.laststar.exchangerates.models.api;

import java.util.Comparator;

import io.realm.RealmObject;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class Item extends RealmObject {
    private String name;
    private double buyUSD;
    private double sellUSD;
    private double buyEUR;
    private double sellEUR;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBuyUSD() {
        return buyUSD;
    }

    public void setBuyUSD(double buyUSD) {
        this.buyUSD = buyUSD;
    }

    public double getSellUSD() {
        return sellUSD;
    }

    public void setSellUSD(double sellUSD) {
        this.sellUSD = sellUSD;
    }

    public double getBuyEUR() {
        return buyEUR;
    }

    public void setBuyEUR(double buyEUR) {
        this.buyEUR = buyEUR;
    }

    public double getSellEUR() {
        return sellEUR;
    }

    public void setSellEUR(double sellEUR) {
        this.sellEUR = sellEUR;
    }

    public static class SortByName implements Comparator<Item> {

        @Override
        public int compare(Item item1, Item item2) {
            return item1.getName().compareTo(item2.getName());
        }
    }
}
