package com.laststar.exchangerates.tools;

import android.content.Context;
import android.util.DisplayMetrics;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class Converter {
    private Context context;

    public Converter(Context context) {
        this.context = context;
    }

    public int dpToPx(int dp) {
        return dp * (context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    private String getValue(double value) {
        return NumberFormat.getInstance(Locale.getDefault()).format(value);
    }

    public String buyText(double value) {
        return "Покупка\n" + getValue(value);
    }

    public String sellText(double value) {
        return "Продажа\n" + getValue(value);
    }

    public String getDDMMYY() {
        String format = "ddMMyy";
        DateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }

    /**
     *
     * @param dateFromDb has 25-Sep-2017 18:00 format
     */
    public String getRatesDate(String dateFromDb) {
        String formatDb = "dd-MMM-yyyy HH:mm";
        String formatNew = "dd MMM, yyyy HH:mm";
        DateFormat dateFormatDb = new SimpleDateFormat(formatDb, Locale.US);
        DateFormat dateFormatNew = new SimpleDateFormat(formatNew, Locale.getDefault());
        Date date = null;
        try {
            date = dateFormatDb.parse(dateFromDb);
            return dateFormatNew.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
