package com.laststar.exchangerates.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.laststar.exchangerates.App;
import com.laststar.exchangerates.api.Api;
import com.laststar.exchangerates.models.api.Item;
import com.laststar.exchangerates.models.api.Rates;
import com.laststar.exchangerates.mvp.views_state.MainAVS;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

@InjectViewState
public class MainAPresenter extends BasePresenter<MainAVS> {
    @Inject
    Api api;

    private String resultString;

    public MainAPresenter() {
        App.getPresenterComponent().inject(this);
    }

    /**
     *
     * @param date has 'ddmmyy' format
     */
    public void getRates(String date) {
        api.getRates("no", date, "yes")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new DisposableObserver<Response<ResponseBody>>() {
                    @Override
                    public void onComplete() {
                        Rates rates = parseResult();
                        if(rates != null) {
                            if (rates.getItems().size() == 0) {
                                getResultsFromLocalDb();
                            } else {
                                getViewState().setResults(rates);
                                updateLocalDb(rates);
                            }
                        } else {
                            getResultsFromLocalDb();
                        }
                    }

                    @Override
                    public void onNext(@NonNull Response<ResponseBody> response) {
                        if(response.isSuccessful()) {
                            try {
                                String result = response.body().string();
                                resultString = result.substring(result.indexOf("{"), result.length());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            getResultsFromLocalDb();
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getResultsFromLocalDb();
                    }
                });
    }

    private Rates parseResult() {
        Rates rates = null;
        try {
            JSONObject object = new JSONObject(resultString);
            JSONArray itemsJson = object.getJSONArray("items");
            rates = new Rates();
            RealmList<Item> items = new RealmList<>();
            for(int i = 0; i < itemsJson.length(); i++) {
                JSONObject itemObject = itemsJson.getJSONObject(i);
                Item item = new Item();
                item.setName(itemObject.getString("name"));
                item.setBuyUSD(itemObject.getString("buyUSD").equals("---") ? 1 : Double.parseDouble(itemObject.getString("buyUSD")));
                item.setSellUSD(itemObject.getString("sellUSD").equals("---") ? 1 : Double.parseDouble(itemObject.getString("sellUSD")));
                item.setBuyEUR(itemObject.getString("buyEUR").equals("---") ? 1 : Double.parseDouble(itemObject.getString("buyEUR")));
                item.setSellEUR(itemObject.getString("sellEUR").equals("---") ? 1 : Double.parseDouble(itemObject.getString("sellEUR")));
                items.add(item);
            }
            Collections.sort(items, new Item.SortByName());
            rates.setItems(items);
            rates.setDate(object.getString("date"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rates;
    }

    private void getResultsFromLocalDb() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Rates> results = realm.where(Rates.class).findAll();
        if(results.size() > 0) {
            Rates rates = results.first();
            if (rates.getItems().size() == 0) {
                getViewState().empty();
            } else {
                getViewState().setResults(rates);
            }
        } else {
            getViewState().empty();
        }
    }

    private void updateLocalDb(Rates rates) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealm(rates);
        realm.commitTransaction();
        realm.close();
    }
}