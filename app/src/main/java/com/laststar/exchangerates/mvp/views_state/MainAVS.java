package com.laststar.exchangerates.mvp.views_state;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.laststar.exchangerates.models.api.Rates;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface MainAVS extends MvpView {
    void setResults(Rates results);
    void empty();
}
