package com.laststar.exchangerates;

import android.app.Application;

import com.laststar.exchangerates.di.components.AppComponent;
import com.laststar.exchangerates.di.components.DaggerAppComponent;
import com.laststar.exchangerates.di.components.DaggerPresenterComponent;
import com.laststar.exchangerates.di.components.PresenterComponent;
import com.laststar.exchangerates.di.modules.ApiModule;
import com.laststar.exchangerates.di.modules.ContextModule;

import io.realm.Realm;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class App extends Application {
    private static AppComponent appComponent;
    private static PresenterComponent presenterComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        initAppComponent();
        initPresenterComponent();
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(getApplicationContext()))
                .build();
    }

    private void initPresenterComponent() {
        presenterComponent = DaggerPresenterComponent.builder()
                .apiModule(new ApiModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static PresenterComponent getPresenterComponent() {
        return presenterComponent;
    }
}
