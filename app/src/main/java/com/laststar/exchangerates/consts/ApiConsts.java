package com.laststar.exchangerates.consts;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public class ApiConsts {
    public static final String HOSTNAME = "http://www.rietumu.ru/";
    public static final String GET_EXCHANGE_RATES = "sng.nsf/RatesXML?OpenAgent";
}
