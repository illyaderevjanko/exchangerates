package com.laststar.exchangerates.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.laststar.exchangerates.consts.ApiConsts;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by last star on 25.September.2017
 * Cruxlab Company,
 * Kharkov, Ukraine.
 */

public interface Api {
    @GET(ApiConsts.HOSTNAME + ApiConsts.GET_EXCHANGE_RATES)
    Observable<Response<ResponseBody>> getRates(@Query("cash") String cash, @Query("rdate") String rdate, @Query("exdate") String exdate);


    class InitApi {
        private static Retrofit retrofit;

        public static Api create() {
            OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

            Gson gson = new GsonBuilder().setLenient().create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConsts.HOSTNAME)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
            return retrofit.create(Api.class);
        }
    }
}
